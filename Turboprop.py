import numpy as np


class Turboprop:

  # IC for Temperature
  def temp(self, number):
    self.Temp = np.array([288.15, 284.9, 281.7, 278.4, 275.2, 271.9, 268.7, 265.4, 262.2,
                          258.9, 255.7, 252.4, 249.2, 245.9, 242.7, 239.5, 236.2, 233.0,
                          229.7, 226.5, 223.3, 220.0, 216.8, 216.7, 216.7, 216.7, 216.7,
                          216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7,
                          216.7, 216.7, 216.7, 216.7, 216.7])
    return(self.Temp[number])

  # IC for Speed
  def speed(self, number):
    self.Speed = np.array([340.3, 338.4, 336.4, 334.5, 332.5, 330.5, 328.6, 326.6, 324.6,
                           322.6, 320.5, 318.5, 316.5, 314.4, 312.3, 310.2, 308.1, 306.0,
                           303.8, 301.7, 299.5, 297.4, 295.2, 295.1, 295.1, 295.1, 295.1,
                           295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1,
                           295.1, 295.1, 295.1, 295.1, 295.1])
    return(self.Speed[number])

  # IC for Pressure
  def pressure(self, number):
    self.Pressure = np.array([101.325, 95.46, 89.88, 84.56, 79.50, 74.69, 70.12, 65.78,
                              61.66, 57.75, 54.05, 50.54, 47.22, 44.08, 41.11, 38.30,
                              35.65, 33.15, 30.80, 28.58, 26.50, 24.54, 22.70, 20.98,
                              19.40, 17.93, 16.58, 15.33, 14.17, 13.10, 12.11, 11.20,
                              10.35, 9.572, 8.850, 8.182, 7.565, 6.995, 6.467, 5.980,
                              5.529])
    return(self.Pressure[number])

  # Going throughout the engine
  def engine_turboprop(self, m_f, T_a, C_a, P_a):
    Cp_a = 1.005  # kJ/kg
    g_a = 1.4
    n_inlet = 0.9
    pr_1 = 1.2  # Axial
    n_axial = 0.8
    pr_2 = 2.5  # Centrifugal
    n_centri = 0.8
    P_loss = 0.05
    Cp_g = 1.148  # kJ/kg
    g_g = 1.333
    n_mech = 0.97
    n_turbine = 0.8
    n_pt = 0.85
    n_nozzle = 0.95
    opd = 0.8  # Optimum Power Division
    n_propeller = 0.8
    n_gearbox = 0.8

    # Inlet
    m_a = 10
    T_01 = T_a + (C_a)**2 / (2 * Cp_a * 1000)
    P_01 = P_a * (1 + n_inlet * (C_a**2) /
                  (2 * 1000 * Cp_a * T_a))**(g_a / (g_a - 1))

    # Axial Compressor
    P_02 = pr_1 * P_01
    dt_1 = (T_01 / n_axial) * ((pr_1)**((g_a - 1) / g_a) - 1)
    T_02 = T_01 + dt_1

    W_axial = Cp_a * (dt_1)
    W_axial_dot = m_a * W_axial

    # Centrifugal Compressor
    P_03 = pr_2 * P_02
    dt_2 = (T_02 / n_centri) * ((pr_2)**((g_a - 1) / g_a) - 1)
    T_03 = T_02 + dt_2

    W_centri = Cp_a * (dt_2)
    W_centri_dot = m_a * W_centri

    # Combustion
    P_04 = P_03 * (1 - P_loss)
    lhv = 43100  # kJ/kg*K
    f = m_f / m_a
    m_g = m_a + m_f
    T_04 = (f * lhv + Cp_a * T_03) / (Cp_g * (1 + f))

    # Axial Turbine
    W_total_dot = W_axial_dot + W_centri_dot
    W_turbine_dot = W_total_dot / n_mech
    W_turbine = W_turbine_dot / m_g

    dt_3 = W_turbine / Cp_g
    T_05 = T_04 - dt_3
    P_05 = P_04 * (1 - (1 / n_turbine) *
                   (1 - (T_05 / T_04)))**(g_g / (g_g - 1))

    # Power Turbine
    P_6 = P_a
    dt4 = opd * n_pt * T_04 * (1 - (1 / (P_04 / P_6))**((g_g - 1) / g_g))
    T_05 = T_04 - dt4
    W_power_turbine = Cp_g * dt4
    W_power_turbine_dot = m_g * Cp_g * dt4
    C_6 = np.sqrt(1000 * Cp_g * dt4)

    # Power Produced
    W_net = n_mech * W_power_turbine
    W_net_dot = n_mech * W_power_turbine_dot
    W_prop = n_propeller * n_gearbox * W_net
    W_prop_dot = n_propeller * n_gearbox * W_net_dot

    # Thrust
    F_prop = W_prop_dot / C_a
    F_jet = m_g * C_6 - m_a * C_a
    F_total = F_prop + F_jet
    print('The total thrust is ' + str(F_total))
    print(' ')

    # Different Powers
    tp = W_prop_dot + F_jet / C_a
    ep = tp / n_propeller
    print('The Equivalent Power is ' + str(ep))
    print(' ')

    # SFC
    SFC = (f * 3600) / ep
    print('The SFC is ' + str(SFC))
    print(' ')

    # Propulsive Efficiency
    n_p = (F_total * C_a) / (0.5 * ((m_g * C_6**2) - (m_a * C_a**2)))
    print('The Propulsive Efficiency is ' + str(n_p))
    print(' ')

    # Efficieny of the Cycle
    n_e = (0.5 * ((m_g * C_6**2) - (m_a * C_a**2))) / (m_f * lhv)
    print('The Efficieny of the Cycle is ' + str(n_e))
    print(' ')

    # Overall Efficiency
    n_0 = (F_total * C_a) / (m_f * lhv)
    print('The Overall Efficiency ' + str(n_0))
    print(' ')
