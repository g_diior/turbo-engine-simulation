import os
import time
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn

MCP_PIN_IN = MCP.P0
channel = none

class TurboFan:

    def __init__(self, i):
        self.Height = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000,
                  5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000,
                  10500, 11000, 11500, 12000, 12500, 13000, 13500, 14000, 14500,
                  15000, 15500, 16000, 16500, 17000, 17500, 18000, 18500, 19000,
                  19500, 20000]

        self.Temp = [288.15, 284.9, 281.7, 278.4, 275.2, 271.9, 268.7, 265.4, 262.2,
                258.9, 255.7, 252.4, 249.2, 245.9, 242.7, 239.5, 236.2, 233.0,
                229.7, 226.5, 223.3, 220.0, 216.8, 216.7, 216.7, 216.7, 216.7,
                216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7,
                216.7, 216.7, 216.7, 216.7, 216.7]

        self.Speed = [340.3, 338.4, 336.4, 334.5, 332.5, 330.5, 328.6, 326.6, 324.6,
                 322.6, 320.5, 318.5, 316.5, 314.4, 312.3, 310.2, 308.1, 306.0,
                 303.8, 301.7, 299.5, 297.4, 295.2, 295.1, 295.1, 295.1, 295.1,
                 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1,
                 295.1, 295.1, 295.1, 295.1, 295.1]

        self.Pressure = [101.325, 95.46, 89.88, 84.56, 79.50, 74.69, 70.12, 65.78,
                    61.66, 57.75, 54.05, 50.54, 47.22, 44.08, 41.11, 38.30,
                    35.65, 33.15, 30.80, 28.58, 26.50, 24.54, 22.70, 20.98,
                    19.40, 17.93, 16.58, 15.33, 14.17, 13.10, 12.11, 11.20,
                    10.35, 9.572, 8.850, 8.182, 7.565, 6.995, 6.467, 5.980,
                    5.529]
        self.global_dict = get_variables(i)
        init_MCP()


    def init_MCP(self):
        # create the spi bus
        spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

        # create the cs (chip select)
        cs = digitalio.DigitalInOut(board.D22)

        # create the mcp object
        mcp = MCP.MCP3008(spi, cs)

        # create an analog input channel on pin 0
        channel = AnalogIn(mcp, MCP_PIN_IN)
        #while(true): print(channel.voltage)

    def normalize_adc_value(self):
        #Input range from min to max
        # normalized_value = (input - min)/(max-min) * desired_range
        # normalized_value = (input - min)/(max-min) * 21
        #if (input >= max) input = max
        #if (input <= min) input = min
        pass

    def get_variables(self, i):
        return {"height": Height[i], "temp": Temp[i], "speed": Speed[i], "pressure": Pressure[i]}



    #Turbofan

    #Variables/Constants
    Cp_a = 1.005        #kJ/kg
    g_a = 1.4
    B = 7.6    #Bypass
    n_inlet = 0.9
    PR_f = 1.75 #Fan
    n_fan = 0.8
    PR_1 = 1.2  #Axial
    n_axial = 0.8
    PR_2 = 2.5  #Centrifugal
    n_centri = 0.8
    P_loss = 0.05
    Cp_g = 1.148        #kJ/kg
    g_g = 1.333
    n_mech = 0.97
    n_LPT = 0.8
    n_HPT = 0.8
    n_nozzle = 0.95   #Cold and Hot Nozzle have same efficency

    def Altitude (input_value):

        Hieght = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000,
                  5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000,
                  10500, 11000, 11500, 12000, 12500, 13000, 13500, 14000, 14500,
                  15000, 15500, 16000, 16500, 17000, 17500, 18000, 18500, 19000,
                  19500, 20000]
        return (adjustment)

    def temperature (adjustment):
        Temp = [288.15, 284.9, 281.7, 278.4, 275.2, 271.9, 268.7, 265.4, 262.2,
                258.9, 255.7, 252.4, 249.2, 245.9, 242.7, 239.5, 236.2, 233.0,
                229.7, 226.5, 223.3, 220.0, 216.8, 216.7, 216.7, 216.7, 216.7,
                216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7, 216.7,
                216.7, 216.7, 216.7, 216.7, 216.7]
        return (Temp[i])

    def speed (adjustment):
        Speed = [340.3, 338.4, 336.4, 334.5, 332.5, 330.5, 328.6, 326.6, 324.6,
                 322.6, 320.5, 318.5, 316.5, 314.4, 312.3, 310.2, 308.1, 306.0,
                 303.8, 301.7, 299.5, 297.4, 295.2, 295.1, 295.1, 295.1, 295.1,
                 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1, 295.1,
                 295.1, 295.1, 295.1, 295.1, 295.1]
        return (Speed[i])

    def pressure (adjustment):
        Pressure = [101.325, 95.46, 89.88, 84.56, 79.50, 74.69, 70.12, 65.78,
                    61.66, 57.75, 54.05, 50.54, 47.22, 44.08, 41.11, 38.30,
                    35.65, 33.15, 30.80, 28.58, 26.50, 24.54, 22.70, 20.98,
                    19.40, 17.93, 16.58, 15.33, 14.17, 13.10, 12.11, 11.20,
                    10.35, 9.572, 8.850, 8.182, 7.565, 6.995, 6.467, 5.980,
                    5.529]
        return (Pressure[i])

    def inlet(self):
        pass
    #Inlet
    T_a = self.global_dict['temperature']
    P_a = pressure()
    C_a = speed()
    m_a = 10
    #TODO: change from none
    m_f = none
    T_01 = T_a + (C_a)**2/(2*Cp_a*1000)
    P_01 = P_a*(1 + n_inlet*(C_a**2)/(2*1000*Cp_a*T_a))**(g_a/(g_a-1))

    #Fan
    P_02 = PR_f*P_01
    dt_1 = (T_01/n_fan)*((PR_1)**((g_a-1)/g_a) - 1)
    T_02 = T_01 + dt_1
    m_c = (m_a*B)/(B+1)
    m_h = (m_a)/(B+1)

    W_fan = Cp_a*(dt_1)
    W_fan_dot = m_a*W_fan

    #Axial Compressor
    P_03 = PR_1*P_02
    dt_2 = (T_02/n_axial)*((PR_1)**((g_a-1)/g_a) - 1)
    T_03 = T_02 + dt_2

    W_axial = Cp_a*(dt_2)
    W_axial_dot = m_h*W_axial

    #Centrifugal Compressor
    P_04 = PR_2*P_03
    dt_3 = (T_03/n_centri)*((PR_2)**((g_a-1)/g_a) - 1)
    T_04 = T_03 + dt_3

    W_centri = Cp_a*(dt_3)
    W_centri_dot = m_h*W_centri

    #Combustion
    P_05 = P_04*(1-P_loss)
    LHV = 43100   #kJ/kg*K
    f = m_f/m_a
    m_hg = m_h + m_f
    T_05 = (f*LHV + Cp_a*T_04)/(Cp_g*(1+f))

    #High Pressure Turbine (STOPPED HERE)
    W_HPT_dot = W_centri_dot/n_mech
    W_HPT = W_HPT_dot/m_hg

    dt_4 = W_HPT/Cp_g
    T_06 = T_05 - dt_4
    P_06 = P_05*(1-(1/n_HPT)*(1-(T_06/T_05)))**(g_g/(g_g-1))

    #Low Pressure Turbine
    W_total_dot = W_axial_dot + W_fan_dot
    W_LPT_dot = W_total_dot/n_mech
    W_LPT = W_LPT_dot/m_hg

    dt_5 = W_LPT/Cp_g
    T_07 = T_06 - dt_5
    P_07 = P_06*(1-(1/n_LPT)*(1-(T_07/T_06)))**(g_g/(g_g-1))

    #Cold Nozzle
    P_a_ratio = P_02/P_a
    P_c_ratio = 1/((1-(1/n_nozzle)*(g_g-1/(g_g+1)))**(g_g/(g_g-1)))
    if (P_a_ratio > P_c_ratio):     #Nozzle Choked
        Ma = 1
        P_2 = P_02/P_c_ratio
        T_09 = T_02
        T_9 = T_09/(1+(g_g-1)/2)
        C_9 = 1*sqrt(g_a*287*T_9)
        Nozzle_cold = 1
        print('The Cold Nozzle is choked')
        print(' ')

    else:                           #Nozzle Not Choked
        P_2 = P_a
        T_09 = T_02
        dt_9 = n_nozzle*T_09*(1-(P_2/P_02))**((g_g-1)/g_g)
        T_9 = T_09 - dt_9
        C_9 = sqrt(2*Cp_g*1000*dt_9)
        Nozzle_cold = 0
        print('The Cold Nozzle is not choked')
        print(' ')

    #Hot Nozzle
    P_a_ratio = P_07/P_a
    P_c_ratio = 1/((1-(1/n_nozzle)*(g_g-1/(g_g+1)))**(g_g/(g_g-1)))
    if (P_a_ratio > P_c_ratio):     #Nozzle Choked
        Ma = 1
        P_8 = P_07/P_c_ratio
        T_08 = T_07
        T_8 = T_08/(1+(g_g-1)/2)
        C_8 = 1*sqrt(g_a*287*T_8)
        Nozzle_hot = 1
        print('The Hot Nozzle is choked')
        print(' ')

    else:                           #Nozzle Not Choked
        P_8 = P_a
        T_08 = T_07
        dt_8 = n_nozzle*T_08*(1-(P_7/P_07))**((g_g-1)/g-g)
        T_8 = T_08 - dt_8
        C_8 = sqrt(2*Cp_g*1000*dt_8)
        Nozzle_hot = 0
        print('The Hot Nozzle is not choked')
        print(' ')

    #Thrust Hot Nozzle
    if (Nozzle_hot == 1):
        rho = P_8/(287*T_8)
        A = m_hg/(rho*C_8)
        F_hot = m_hg*C_8 - m_a*C_a + (P_8 - P_a)*A

    else:
        F_hot = m_hg*C_8 - m_a*C_a

    #Thrust Cold Nozzle
    if (Nozzle_cold == 1):    #Nozzle Choked
        rho = P_9/(287*T_9)
        A = m_c/(rho*C_9)
        F_cold = m_c*C_9 - m_a*C_a + (P_9 - P_a)*A

    else:                     #Nozzle Not Choked
        F_cold = m_c*C_9

    #SFC
    F = F_hot + F_cold
    SFC = m_f/F
    print('The total thrust is '+ str(F))
    print(' ')
    print('The SFC is '+ str(SFC))
    print(' ')

    #Propulsive Efficiency
    n_p = (F_total*C_a)/(0.5*((m_g*C_6**2)-(m_a*C_a**2)))
    print('The Propulsive Efficiency is '+ str(n_p))
    print(' ')

    #Efficieny of the Cycle
    n_e = (0.5*((m_g*C_6**2)-(m_a*C_a**2)))/(m_f*LHV)
    print('The Efficieny of the Cycle is '+ str(n_e))
    print(' ')

    #Overall Efficiency
    n_0 = (F_total*C_a)/(m_f*LHV)
    print('The Overall Efficiency '+ str(n_0))
    print(' ')
