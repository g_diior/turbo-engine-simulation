import numpy as np
from Turbojet import Turbojet
from Turboprop import Turboprop
from Turbofan_1 import Turbofan

# Welcome
print("Hello and welcome to the automatic Aerodynamic Cycle calculator for Turbojet, Turboprop and Turbofan")
print(" ")

# Height Levels for all engines
Height = np.array([0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000,
                   5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000,
                   10500, 11000, 11500, 12000, 12500, 13000, 13500, 14000, 14500,
                   15000, 15500, 16000, 16500, 17000, 17500, 18000, 18500, 19000,
                   19500, 20000])

# Enter which model is being simulated
print("To model the Turbojet enter 1")
print("To model the Turboprop enter 2")
print("To model the Turbofan enter 3")
model = int(input("Which Engine Model would you like to simulate? "))


if model == 1:
    # Run Turbojet
    print(" ")
    print("The Turbojet model is running")
    print(" ")

    # Input two parameters from the electronics
    flight_level = float(input("What is your altitude (in meters) "))
    print(" ")
    fuel_level = float(input("What is your fuel flow rate (in kg/s) "))
    print(" ")
    m_f = fuel_level
    print("The altitude is " + str(flight_level) +
          " meters and the fuel flow is " + str(m_f) + "kg/s ")

    # Match the hieght to a certain location
    for i in range(41):
        if flight_level == Height[i]:
            n = i
            print("The flight level is " + str(Height[i]))
            print(n)
            break

    # Find other IC's
    turbo_jet = Turbojet()
    T_a = turbo_jet.temp(n)
    C_a = turbo_jet.speed(n)
    P_a = turbo_jet.pressure(n)
    print("The IC for Temperature is " + str(T_a))
    print("The IC for Speed is " + str(C_a))
    print("The IC for Pressure is " + str(P_a))
    print("")

    # Go throughout the engine to determine all other parameters
    Engine_running = turbo_jet.engine_turbojet(m_f, T_a, C_a, P_a)

elif model == 2:
    # Run Turboprop
    print(" ")
    print("The Turboprop model is running")
    print(" ")

    # Input two parameters from the electronics
    flight_level = float(input("What is your altitude (in meters) "))
    print(" ")
    fuel_level = float(input("What is your fuel flow rate (in kg/s) "))
    print(" ")
    m_f = fuel_level
    print("The altitude is " + str(flight_level) +
          " meters and the fuel flow is " + str(m_f) + "kg/s ")

    # Match the hieght to a certain location
    for i in range(41):
        if flight_level == Height[i]:
            n = i
            print("The flight level is " + str(Height[i]))
            print(n)
            break

    # Find other IC's
    turbo_prop = Turboprop()
    T_a = turbo_prop.temp(n)
    C_a = turbo_prop.speed(n)
    P_a = turbo_prop.pressure(n)
    print("The IC for Temperature is " + str(T_a))
    print("The IC for Speed is " + str(C_a))
    print("The IC for Pressure is " + str(P_a))
    print("")

    # Go throughout the engine to determine all other parameters
    Engine_running = turbo_prop.engine_turboprop(m_f, T_a, C_a, P_a)

elif model == 3:
    # Run Turbofan
    print(" ")
    print("The Turbofan model is running")
    print(" ")

    # Input two parameters from the electronics
    flight_level = float(input("What is your altitude (in meters) "))
    print(" ")
    fuel_level = float(input("What is your fuel flow rate (in kg/s) "))
    print(" ")
    m_f = fuel_level
    print("The altitude is " + str(flight_level) +
          " meters and the fuel flow is " + str(m_f) + "kg/s ")

    # Match the hieght to a certain location
    for i in range(41):
        if flight_level == Height[i]:
            n = i
            print("The flight level is " + str(Height[i]))
            print(n)
            break

    # Find other IC's
    turbo_fan = Turbofan()
    T_a = turbo_fan.temp(n)
    C_a = turbo_fan.speed(n)
    P_a = turbo_fan.pressure(n)
    print("The IC for Temperature is " + str(T_a))
    print("The IC for Speed is " + str(C_a))
    print("The IC for Pressure is " + str(P_a))
    print("")

    # Go throughout the engine to determine all other parameters
    Engine_running = turbo_fan.engine_turbofan(m_f, T_a, C_a, P_a)

else:
    print(" ")
    print("Non-valid number entered")
    print(" ")
